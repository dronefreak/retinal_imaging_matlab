# README #

The code detects bright spots in images using basic image processing techniques. Can be used to detect retinal dysfunctions.

### Setup ###

* Clone the repo
* Run the code in MATLAB
* You should see a result similar to  ![Detection](https://bytebucket.org/dronefreak/retinal_imaging_matlab/raw/8a70f791d99e13d284460e5e02666da431fd12bf/test_image.png)



### Contribution guidelines ###

* Writing tests
* Code review